angular.module('picmonicSoundboard.services.soundboardmedia', [])

.factory('SoundboardMedia', [function() {
  var obj = {
    media: [
      {
        'title': 'Big Sean - I don wanna fuck wit joo',
        'image': 'img/picmonic_soundboard/big-sean-i-dont-wanna-fuck-wit-joo.jpg',
        'desc': 'Big Sean - I dont wann fuck wit joo',
        'file': '/sounds/picmonic_sounds/big-sean-i-dont-wanna-fuck-wit-joo.mp3'
      },
      {
        'title': 'Bonesaw Is Ready',
        'image': 'img/picmonic_soundboard/bonesaw-is-ready.jpeg',
        'desc': 'Bonesaw Is Ready',
        'file': '/sounds/picmonic_sounds/bonesaw-is-ready.mp3'
      },
      {
        'title': 'Bubbles - Nice Kitty',
        'image': 'img/picmonic_soundboard/bubbles-storm-drain.jpg',
        'desc': 'Bubbles - Nice Kitty',
        'file': '/sounds/picmonic_sounds/bubbles-storm-drain.mp3'
      },
      {
        'title': 'Bubbles - Yes',
        'image': 'img/picmonic_soundboard/bubbles-yes.png',
        'desc': 'Bubbles - Yes',
        'file': '/sounds/picmonic_sounds/bubbles-yes.mp3'
      },
      {
        'title': 'Cash Money',
        'image': 'img/picmonic_soundboard/cash-money.png',
        'desc': 'Cash Money',
        'file': '/sounds/picmonic_sounds/cash-money.mp3'
      },
      {
        'title': 'Diego Yay',
        'image': 'img/picmonic_soundboard/diego-yay.png',
        'desc': 'Diego Yay',
        'file': '/sounds/picmonic_sounds/diego-yay.mp3'
      },
      {
        'title': 'Hajibaba',
        'image': 'img/picmonic_soundboard/hajibaba.jpeg',
        'desc': 'Hajibaba',
        'file': '/sounds/picmonic_sounds/hajibaba.mp3'
      },
      {
        'title': 'He Man Globe',
        'image': 'img/picmonic_soundboard/he-man-globe.jpg',
        'desc': 'He Man Globe',
        'file': '/sounds/picmonic_sounds/he-man-globe.mp3'
      },
      {
        'title': 'Heroic',
        'image': 'img/picmonic_soundboard/heroic.png',
        'desc': 'Heroic',
        'file': '/sounds/picmonic_sounds/heroic.mp3'
      },
      {
        'title': 'Im in love wit da coco',
        'image': 'img/picmonic_soundboard/im-in-love-wit-da-co-co.jpg',
        'desc': 'Im in love wit da coco',
        'file': '/sounds/picmonic_sounds/im-in-love-wit-da-co-co.mp3'
      },
      {
        'title': 'Law and Order',
        'image': 'img/picmonic_soundboard/law-and-order.png',
        'desc': 'Law and Order',
        'file': '/sounds/picmonic_sounds/law-and-order.mp3'
      },
      {
        'title': 'Matt "Straight Out Of Compton" Walker',
        'image': 'img/picmonic_soundboard/matt-tadpoles.jpg',
        'desc': 'Matt "Straight Out Of Compton" Walker',
        'file': '/sounds/picmonic_sounds/matt-tadpoles.mp3'
      },
      {
        'title': 'Rap Air Horn',
        'image': 'img/picmonic_soundboard/instant-rap-air-horn.png',
        'desc': 'Rap Air Horn',
        'file': '/sounds/picmonic_sounds/instant-rap-air-horn.mp3'
      },
      {
        'title': 'Message for RON OR KEN',
        'image': 'img/picmonic_soundboard/message-for-ken-or-ron.jpg',
        'desc': 'Message for RON OR KEN',
        'file': '/sounds/picmonic_sounds/message-for-ken-or-ron.mp3'
      },
      {
        'title': 'Nicki Minaj Ass',
        'image': 'img/picmonic_soundboard/nicki-minaj-ass.png',
        'desc': 'Nicki Minaj Ass',
        'file': '/sounds/picmonic_sounds/nicki-minaj-ass.mp3'
      },
      {
        'title': 'Shame Bell',
        'image': 'img/picmonic_soundboard/shame-bell.png',
        'desc': 'Shame Bell',
        'file': '/sounds/picmonic_sounds/shame-bell.mp3'
      },
      {
        'title': 'Silly Yak',
        'image': 'img/picmonic_soundboard/silly-yak.png',
        'desc': 'Silly Yak',
        'file': '/sounds/picmonic_sounds/silly-yak.mp3'
      }
    ],

    getAllMedia: function() {
      return this.media;
    }

  };

  return obj;
}]);

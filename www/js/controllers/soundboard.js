angular.module('picmonicSoundboard.controllers.soundboard', [])

.controller('SoundboardCtrl', ['$scope', '$window', 'SoundboardMedia', function ($scope, $window, SoundboardMedia) {
  $scope.media = null;

  $scope.vm = {
    showDelete: false,
    showMove: false,
    sounds: angular.copy(SoundboardMedia.getAllMedia())
  };

  $scope.play = function (sound) {
    if ($scope.media) {
      $scope.media.pause();
    }

    if ($window.cordova) {
      ionic.Platform.ready(function () {

        var src = sound.file;
        if (ionic.Platform.is('android')) {
          src = '/android_asset/www/' + src;
        }
        $scope.media = new $window.Media(src);
        $scope.media.play();
      });
    } else {
      $scope.media = new Audio();
      $scope.media.src = sound.file;
      $scope.media.load();
      $scope.media.play();
    }

  }; // ***END play()

  $scope.deleteItem = function(index) {
    $scope.vm.sounds.splice(index, 1);
  };

  $scope.refreshSoundList = function() {
    $scope.vm.sounds = angular.copy(SoundboardMedia.getAllMedia());
    // Stop the ion-refresher from spinning
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.moveSound = function(sound, fromIndex, toIndex) {
    $scope.vm.sounds.splice(fromIndex, 1);
    $scope.vm.sounds.splice(toIndex, 0, sound);
  };

}]);
